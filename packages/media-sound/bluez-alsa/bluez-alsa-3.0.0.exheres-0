# Copyright 2020 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require \
    autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ] \
    github [ user=Arkq tag=v${PV} ] test-dbus-daemon

SUMMARY="A direct integration between BlueZ and ALSA, allowing to use bluetooth audio devices with ALSA."

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/glib:2[>=2.30]
        dev-libs/libbsd
        media-libs/sbc[>=1.2]
        net-wireless/bluez[>=5.0]
        sys-apps/dbus[>=1.6]
        sys-libs/ncurses
        sys-sound/alsa-lib
    run:
        user/bluealsa
    test:
        dev-libs/check[>=0.9.10]
"

# Unimplemented options:
#  aac:     --enable-aac,       media-libs/fdk-aac[>=0.1.1]
#  aptx:    --enable-aptx,      media-libs/libopenaptx[>=1.2.0] (missing)
#  aptx-hd: --enable-aptx-hd,   openaptxhd[>=1.2.0] (missing)
#  ldac:    --enable-ldac,      media-libs/libldac[>=2.0.0]
#  mp3?:    --enable-mp3lame,   media-sound/lame
#  mp3?:    --enable-mpg123,    media-sound/mpg123[>=1.0.0]
#  doc:     --enable-manpages,  pandoc

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # no dependencies
    --enable-a2dpconf
    --enable-msbc
    --enable-ofono
    --enable-upower

    # common dependencies
    --enable-hcitop
)

DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-test --disable-test'
)

src_install() {
    default

    insinto /etc/dbus-1/system.d
    doins "${FILES}"/bluealsa-exherbo.conf
}

